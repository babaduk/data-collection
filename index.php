<?php
$f3 = require_once __DIR__ . '/lib/f3/base.php';
require_once __DIR__ . '/controllers/PlayerController.php';


function glob_call($f3,$params){
    $player_controller = new PlayerController($f3,$params);
    $player_controller->displayForm();
}

$f3->route('GET|POST /project_id/@project_id',
    'glob_call'
);




$f3->run();
?>