<?php
require_once __DIR__ . '/../lib/Player.php';


class PlayerController{
    
    public $player;
    private $f3;
    
    function __construct($f3,$params){
        
    $this->f3  = $f3;
        
    $project_id = $params["project_id"];
    $resp_id = $f3->get("REQUEST.resp_id");
    
    
    
    $player = new Player($project_id,$resp_id);
    
    $action = $resp_id = $f3->get("POST.navigation");
    
    if($action==="next"){
        $player->nextPage();  
    }elseif ($action==="prev") {
        $player->prevPage();
    }
    
    $this->player = $player;
    
    }
    
    
    public function displayForm(){
        $this->f3->set('player',$this->player);
        echo \Template::instance()->render('/templates/template.html');
    }
}

?>