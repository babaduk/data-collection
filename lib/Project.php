<?php

require_once __DIR__ . '/rb_dbo.php';
	
	class Project{
		
		public $id;
		public $title;
		
		protected function bind(){
        		$temp_obj = ProjectDBO::find($this->id);
        		$this->id = $temp_obj->id;
        	}
		
	    function __construct($project_id){
	        $this->id = $project_id;
	        
	        $this->bind();
	        
	        $this->title = "The First one";
	    }
	}
	
	class ProjectDBO{
		public static function find($id){
	        	$db_obj = R::findOne('project', ' id = ?', [ $id ] );
        		if($db_obj===NULL){
			            throw new Exception($id . ": Not Found");
		        }
        		return $db_obj;
	    }
	    
	}
?>