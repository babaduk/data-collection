<?php

require_once __DIR__ . '/rb_dbo.php';

class Respondent{
	
		public $id;
		public $project_id;
		public $last_page_id;
	
	    protected function bind(){
        		$resp_temp_obj = RespondentDBO::find($this->id);
        		$this->last_page_id = $resp_temp_obj->last_page_id;
        }
	        
	   function update_last_page($nav_num){
        		$this->last_page_id = RespondentDBO::update_last_page($this->id,$nav_num);
        	}
	        	    
	   function __construct($repsondent_id = NULL,$project_id){
		        $this->id = $repsondent_id;
		        $this->project_id = $project_id;
		        
		        
		        if(is_null( $this->id) || empty($this->id)){
		        	$this->id = RespondentDBO::create_respondent($this->project_id);
		        }
		        
		        $this->bind();
		    }
		}
class RespondentDBO{
			public static function create_respondent($project_id){
        		$db_obj = R::dispense( 'respondent' );
	        	$db_obj->project_id = $project_id;
	        	$id = R::store( $db_obj );
	        	return $id;
	        }
	        
	        public static function find($id){
	        	$db_obj = R::findOne('respondent', ' id = ?', [ $id ] );
        		if($db_obj===NULL){
			            throw new Exception($id . ": Not Found");
		        }
        		return $db_obj;
	        }
	        
	        public static function update_last_page($id,$nav_num){
	        	$db_obj = R::findOne('respondent', ' id = ?', [ $id ] );
	        	$db_obj->last_page_id = $nav_num;
	        	R::store( $db_obj );
	        	return $nav_num;
	        }
	}
	

?>