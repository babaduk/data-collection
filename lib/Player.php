<?php
require_once __DIR__ . '/Project.php';
require_once __DIR__ . '/Page.php';
require_once __DIR__ . '/Respondent.php';

class Player{
	
	
		public $project;
		public $repsondent;
		public $page;
	
	    function __construct($project_id,$repsondent_id){
	    	
	    	
	        $this->project = new Project($project_id);
	        $this->repsondent = new Respondent($repsondent_id,$this->project->id);
	        $this->page = new Page($this->repsondent->last_page_id,$this->project->id);
	        
	        $this->repsondent->last_page_id = $this->page->id;
	    }
	    function nextPage(){
	    	$this->repsondent->update_last_page($this->page->getNextPage());
	    }
	    
	    function prevPage(){
	    	$this->repsondent->update_last_page($this->page->getPrevPage());
	    }
	    
	}
?>