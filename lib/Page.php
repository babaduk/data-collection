<?php

require_once __DIR__ . '/rb_dbo.php';

    class Page{
        function __construct($page_id,$project_id){
            
            $this->id = $page_id;
            $this->project_id = $project_id;
            if($this->id===NULL){
                $this->id = PageDBO::get_first($project_id);    
            }
        }
        
        function getNextPage(){
            $this->id = PageDBO::get_next($this->id,$this->project_id);
            return $this->id;
        }
        
        function getPrevPage(){
            $this->id = PageDBO::get_prev($this->id,$this->project_id);
            return $this->id;
        }
        
    }
    
    class PageDBO{    
        public static function get_first($project_id){
            $first_id = R::getCell("
            			SELECT id from page
                        where project_id = ?
                        order by order_id asc
                        limit 1
                        ",[$project_id]);
            return $first_id;
        }
        
        public static function get_last($project_id){
            $last_id =	R::getCell("
            			SELECT id from page
                        where project_id = ?
                        order by order_id desc
                        limit 1
                        ",[$project_id]);
            return $last_id;
        }
        
        public static function get_next($id,$project_id){
            $next_id =	R::getCell("
            			select id from page where order_id  > 
                        (select order_id from page where id = ? and project_id = ? )
                        and project_id = ?
                        order by order_id asc
                        limit 1
                        ",[$id,$project_id,$project_id]);
            if($next_id===NULL){
                return PageDBO::get_last($project_id);
            }
            return $next_id;
        }
        
        
        public static function get_prev($id,$project_id){
            $prev_id =	R::getCell("
            			select id from page where order_id  < 
                        (select order_id from page where id = ? and project_id = ? )
                        and project_id = ?
                        order by order_id desc
                        limit 1
                        ",[$id,$project_id,$project_id]);
            if($prev_id===NULL){
                return PageDBO::get_first($project_id);
            }
            return $prev_id;
        }
        
    }
?>
